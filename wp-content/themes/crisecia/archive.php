<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package crisecia
 */

get_header();
?>

	<!-- PG INICIAL -->
	<div class="pg pg-inicial">  
		<!-- CONTAINER BOOTSTRAP -->
		<div class="containerFull">
		
			<!-- ROW SESSÃO DE POSTS -->
			<div class="row">
				<div class="col-md-12">
					<!-- SESSÃO DE POSTS -->
					<section class="sessaoPosts">
						<?php 
					        
							if ( have_posts() ) : while( have_posts() ) : the_post();
								$fotoDestaquePost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								
								$fotoDestaquePost = $fotoDestaquePost[0];
								
								global $post;
								
								$categories = get_the_category();
						?>
							<!-- POST -->
							<div class="postPaginaInicial">
								<!-- IMAGEM DESTAQUE DO BANNER -->
								<a href="<?php echo get_the_permalink(); ?>">
									<figure style="background: url(<?php echo $fotoDestaquePost; ?>);">
										<img src="<?php echo $fotoDestaquePost; ?>" alt="Imagem">
									</figure>
								</a>	
								<!-- INFORMAÇÕES DO POST -->
								<article>
									<!-- CATEGORIA DO POST -->
									<?php 
										foreach ($categories as $categories){
											if ($categories->name != "destaque" && $categories->name != "Sem categoria" ){
												$nomeCategoria = $categories->name;
											}
										} 
									?>
									<a href="<?php echo get_the_permalink(); ?>"><h3><?php echo $nomeCategoria; ?></h3></a>
									<!-- TÍTULO DO POST-->
									<a href="<?php echo get_the_permalink(); ?>"><h2><?php echo get_the_title(); ?></h2></a>
									<!-- DATA DO POST -->
									<span class="dataDoPost"><?php echo  get_the_date('j F, Y'); ?></span>
									<!-- DESCRIÇÃO DO POST -->
									<p><?php customExcerpt(200); ?></p>
								</article>
							</div>
						<?php endwhile;endif; wp_reset_query(); ?>
						
					</section>
				</div>
				
			</div>
		</div>
	</div>

<?php

get_footer();
