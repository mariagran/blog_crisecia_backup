<?php
/**
 * Template Name: Quem Somos
 * Description: Quem Somos
 *
 * @package crisecia
 */
get_header(); ?>
<div class="pg pg-quemSomos">
		
	<section class="historiaQuemSomos">
		<div class="areaTextoQuemSomos">
			<div class="textoQuemSomos">
				<h4>Nossa História </h4>
				<p>A Cris & Cia Moda Country nasceu no ano de 2003 com o objetivo de identificar, selecionar e colocar à disposição dos consumidores e clientes as melhores marcas do mercado nacional e internacional.
					Oferecemos a maior variedade de artigos western do varejo on-line. São muitos itens distribuídos em diversas categorias. Nossa equipe de colaboradores é formada por profissionais treinados que além de tudo amam o segmento western deixando nossos clientes com a satisfação garantida e estarão sempre ao seu lado.</p>
			</div>
		</div>
	</section>

	<section class="nossaPaixao">
		<div class="textoNossaPainxao">
			<h4>Nossa Paixão</h4>
			<p>Somos uma empresa de gente apaixonada pelo que faz. Amamos a vida no campo e também não abrimos mão da vida na cidade. A verdade é que curtimos o melhor de cada um. Por isso transferimos pra dentro de nossa loja exatamente este sentimento: o melhor da moda country!</p>
		</div>
	</section>

	<section class="areaNossaLoja">
		<div class="areaTitulo">
			<h4>Nossa Loja</h4>
			<p>Somos uma empresa de gente apaixonada pelo que faz. Amamos a vida no campo e também não abrimos mão da vida na cidade. A verdade é que curtimos o melhor de cada um. Por isso transferimos pra dentro de nossa loja exatamente este sentimento: o melhor da moda country!</p>
		</div>
		
		<div class="areaFotos">
			<div class="containerConteudo">
				<img src="http://recursos.Crisecia-hlog.ecommercestore.com.br/i/imgSite/nossaLoja.png" alt="">
			</div>
		</div>
	</section>
		
	<section class="mapa">
	    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3694.03293603303!2d-47.39213058504702!3d-22.200854585375286!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94c80b728f5f0373%3A0x1fdc41fabcb71ee!2sCris+e+Cia+moda+country!5e0!3m2!1spt-BR!2sbr!4v1526395586347"  frameborder="0" style="border:0" allowfullscreen></iframe>
	</section>
		
	<section class="areaNossosNumeros">
		<div class="textoNossaPainxao">
			<h4>Nossa Números</h4>
		</div>

		<ul>
			<li>
				<img src="http://recursos.Crisecia-hlog.ecommercestore.com.br/i/imgSite/satisfacao.png" alt="">
				<span>90%</span>
				<p>de satisfação dos nossos clientes</p>
			</li>
			<li>
				<img src="http://recursos.Crisecia-hlog.ecommercestore.com.br/i/imgSite/pedidos.png" alt="">
				<span>16 Mil</span>
				<p>pedidos enviados</p>
			</li>
			<li>
				<img src="http://recursos.Crisecia-hlog.ecommercestore.com.br/i/imgSite/marcas.png" alt="">
				<span>50</span>
				<p>marcas  exclusivas</p>
			</li>
		</ul>
	</section>

	<section class="areaParceiros">
		<div class="row">
			<div class="col-md-6">
				<div class="caixaTexto">
					<div class="textoNossaParceiros">
						<h4>Marcas Exclusivas</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dignissim, odio quis pharetra efficitur, justo erat facilisis quam, sed tempus nibh lectus ut ligula. Aenean tempus mattis massa, in consequat lacus pretium at. Duis cursus finibus bibendum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent euismod neque in maximus auctor. </p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<figure>
					<div class="carrossel">
						<img src="http://recursos.Crisecia-hlog.ecommercestore.com.br/i/imgSite/chapeuProduto.png" alt="">
					</div>
				</figure>
			</div>
		</div>
	</section>

	<section class="areaDepoiemtos">
		<div class="textoNossaDepoimentos">
			<h4>Depoimentos</h4>
		</div>
		<ul>
			<li>
				<div class="row">
					<div class="col-md-3">
						<figure>
							<img src="http://recursos.Crisecia-hlog.ecommercestore.com.br/i/imgSite/dep1.png" alt="">
						</figure>
					</div>
					<div class="col-md-9">
						<div class="texto">
							<h2>Bruno Viga</h2>
							<p>"Atendimento excelente, chegou antes do prazo."</p>
							<span>18/01/2018 via Facebook</span>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="row">
					<div class="col-md-3">
						<figure>
							<img src="http://recursos.Crisecia-hlog.ecommercestore.com.br/i/imgSite/dep2.png" alt="">
						</figure>
					</div>
					<div class="col-md-9">
						<div class="texto">
							<h2>Flávia Santos</h2>
							<p>"Melhor loja country, preço e qualidade excelente e vendedores atenciosos."</p>
							<span>18/01/2018 via Facebook</span>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="row">
					<div class="col-md-3">
						<figure>
							<img src="http://recursos.Crisecia-hlog.ecommercestore.com.br/i/imgSite/foto.png" alt="">
						</figure>
					</div>
					<div class="col-md-9">
						<div class="texto">
							<h2>Cláudia Padovan</h2>
							<p>"Ontem recebi o 2o chapéu que comprei pela internet nesta loja. Muito satisfeita com o atendimento, entrega e o cuidado da embalagem, fora a qualidade do produto. Super indico esta loja!"</p>
							<span>28/02/2018 via Facebook</span>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="row">
					<div class="col-md-3">
						<figure>
							<img src="http://recursos.Crisecia-hlog.ecommercestore.com.br/i/imgSite/dep3.png" alt="">
						</figure>
					</div>
					<div class="col-md-9">
						<div class="texto">
							<h2>Will Han</h2>
							<p>"Excelente atendimento, material de primeira linha, perfeito para quem tem paixão por moda country."</p>
							<span>09/11/2017 via Facebook</span>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</section>
	
	<section class="areaOqueestaoFalando">
		<div class="textoOqueEstaoFalando">
			<h4>O Que estão falando sobre a Cris E Cia </h4>
		</div>

		<div class="conteudo">
			<iframe  src="https://www.youtube.com/embed/x4g0lRtp9EI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		</div>
	</section>
</div>
<?php get_footer(); ?>