<?php

/**

 * The header for our theme

 *

 * This is the template that displays all of the <head> section and everything up until <div id="content">

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package crisecia

 */

global $configuracao;

?>

<!doctype html>

<html <?php language_attributes(); ?>>

<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="https://gmpg.org/xfn/11">



	<!-- FAVICON -->

	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $configuracao['opt_favicon'] ?>" /> 



	<?php
		echo $configuracao['header_scripts'];
		wp_head(); 
	?>

</head>



<body <?php body_class(); ?> >
	<?php echo $configuracao['header_body']; ?>
	<div class="areaContatoTop" style="background: #323232;text-align: right;">
             <div class="atendimento">
                <a href="tel:(19) 3555-1607" class="telefone">(19) 3555-1607</a>
                <a href="tel:(19) 9.9713-0537" class="whatsapp">(19) 9.9713-0537</a>
                  <span>Atendimento das 9h ás 18h - Sab das 9h ás 14hs</span>
            </div>
        </div>
        <!-- TOPO -->
        <header class="topo">
            <div class="containerFullLarge">
                <div class="row menuSearchLogo">
                    <!-- LOGO -->
                    <div class="col-md-2 areaLogo">
                       <div class="buttonMobile">
                           <span></span>
                           <span></span>
                           <span></span>
                       </div>
                        <a href="<?php echo get_home_url(); ?>">
                            <img class="logo" src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="Logo Chris &amp; Cia">
                        </a>
                        <a href="https://checkout.crisecia.com.br/MinhaConta/Dados" class="loginUserSite">
                            <div class="userLogin"></div>
                        </a>
                    </div> 
                    <!-- MENU  -->  
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-8">          
                                <div class="searchBusca">
                                    <input type="" placeholder="DIGITE O QUE VOCÊ PROCURA AQUI..." name="">
                                    <span id="btnSearchPesquisa"></span> 
                                </div>         
                            </div>  
                            <div class="col-md-4">          
                                <div class="logar">
                                   <div class="imgLogar">
                                       
                                   </div>
                                   <div class="linksLogar">
                                       <span class="areaEntrarLinks"><a href="https://checkout.crisecia.com.br/MinhaConta/Dados">Entre</a>  <br>  ou <a href="https://checkout.crisecia.com.br/MinhaConta/Pedido/" class="fbits-link-minhaConta">
                                            Meus Pedidos</a>
                                        </span>
                                   </div>
                                </div>         
                            </div>                
                        </div>
                    </div>
                </div>

                <div class="navbar" role="navigation">  
                           
                            <!--  MENU MOBILE-->
                            <div class="menu">
                                <p>Categorias</p>
                                <span id="fecharMenuMobile"></span>
                                <ul class="menu">
                                    <li class="item  raiz item0 menu-geral">
                                         <a href="https://www.crisecia.com.br/chapeus" class="menu-geral">
                                            <img src="https://recursos.crisecia.com.br/Imagem/Menu/296/Chapéus.png">Chapéus
                                        </a>
                                        <span class="arrowmenu"></span><ul class="filho  item0">
                                            <li class="item  item5 menu-geral-filho">
                                            <span class="menu-geral-filho">Marca </span>
                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  item5">
                                                <li class="item  item7 menu-geral-neto">
                                                    <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AAmerican+Hat" class="menu-geral-neto">American Hat  </a>
                                                 </li>          
                                                    <li class="item  item7 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3ACharlie+1+Horse" class="menu-geral-neto">Charlie 1 Horse </a>

                                                    </li>          
                                                    <li class="item  item7 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AEldorado" class="menu-geral-neto">Eldorado  </a>

                                                    </li>          
                                                    <li class="item  item7 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3ALone+Star" class="menu-geral-neto">Lone Star  </a>

                                                    </li>          
                                                    <li class="item  item7 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AMexican+Hats" class="menu-geral-neto">Mexican Hats  </a>

                                                    </li>          
                                                    <li class="item  item7 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3APralana" class="menu-geral-neto">Pralana  </a>

                                                    </li>          
                                                    <li class="item  ultimo item7 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AResistol" class="menu-geral-neto">Resistol </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  item5 menu-geral-filho">

                                                                <span class="menu-geral-filho">Material </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  item5">
                                                    <li class="item  item3 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=filtroAttrMaterial14%3AFeltro" class="menu-geral-neto">Feltro </a>

                                                    </li>          
                                                    <li class="item  item3 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=filtroAttrMaterial14%3ALona" class="menu-geral-neto">Lona </a>

                                                    </li>          
                                                    <li class="item  ultimo item3 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=filtroAttrMaterial14%3APalha" class="menu-geral-neto">Palha </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  item5 menu-geral-filho">

                                                                <span class="menu-geral-filho">Acessórios </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  item5">
                                                    <li class="item  item3 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus/acessorios?busca=&amp;ordenacao=&amp;filtro=Categoria%3ABandas" class="menu-geral-neto">Bandas </a>

                                                    </li>          
                                                    <li class="item  item3 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus/acessorios?busca=&amp;ordenacao=&amp;filtro=Categoria%3ACases" class="menu-geral-neto">Cases </a>

                                                    </li>          
                                                    <li class="item  ultimo item3 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus/acessorios?busca=&amp;ordenacao=&amp;filtro=Categoria%3ASuporte" class="menu-geral-neto">Suporte </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  item5 menu-geral-filho">

                                                                <span class="menu-geral-filho">Importados </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  item5">
                                                    <li class="item  item4 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AAmerican+Hat" class="menu-geral-neto">American Hat </a>

                                                    </li>          
                                                    <li class="item  item4 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3ACharlie+1+Horse" class="menu-geral-neto">Charlie 1 Horse </a>

                                                    </li>          
                                                    <li class="item  item4 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3ALone+Star" class="menu-geral-neto">Lone Star </a>

                                                    </li>          
                                                    <li class="item  ultimo item4 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AResistol" class="menu-geral-neto">Resistol </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  ultimo item5 menu-geral-filho">

                                                                <span class="menu-geral-filho">Nacionais </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  ultimo item5">
                                                    <li class="item  item3 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AEldorado" class="menu-geral-neto">Eldorado </a>

                                                    </li>          
                                                    <li class="item  item3 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AMexican+Hats" class="menu-geral-neto">Mexican Hats </a>

                                                    </li>          
                                                    <li class="item  ultimo item3 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3APralana" class="menu-geral-neto">Pralana </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  raiz item0 menu-geral">

                                                            <a href="https://www.crisecia.com.br/botas" class="menu-geral"><img src="https://recursos.crisecia.com.br/Imagem/Menu/297/Botas.png">Botas</a>

                                                            <span class="arrowmenu"></span><ul class="filho  item0">
                                                    <li class="item  item3 menu-geral-filho">

                                                                <span class="arrowmenu arrowmenu-sub"></span><span class="menu-geral-filho">Marca </span>

                                                            <ul class="filho  item3">
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AAriat" class="menu-geral-neto">Ariat </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AClassic" class="menu-geral-neto">Classic </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3ADurango" class="menu-geral-neto">Durango </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AGalope+Boots" class="menu-geral-neto">Galope Boots </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AGoyazes" class="menu-geral-neto">Goyazes </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AHelazza+Boots" class="menu-geral-neto">Helazza Boots </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/busca?busca=Botas+jacomo&amp;ordenacao=DataCadastro%3Adecrescente" class="menu-geral-neto">Jácomo </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AJustin+Boots" class="menu-geral-neto">Justin Boots </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AMexican+Boots" class="menu-geral-neto">Mexican Boots </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AMr.+West+Boots" class="menu-geral-neto">Mr. West Boots </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/rosa-ribeiro" class="menu-geral-neto">Rosa Ribeiro </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3ATexas+Boots" class="menu-geral-neto">Texas Boots </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3ATimberland" class="menu-geral-neto">Timberland </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3ATucson#" class="menu-geral-neto">Tucson </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AVimar+Boots" class="menu-geral-neto">Vimar Boots </a>

                                                    </li>          
                                                    <li class="item  ultimo item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AZebu" class="menu-geral-neto">Zebu </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  item3 menu-geral-filho">

                                                                <span class="menu-geral-filho">Para </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  item3">
                                                    <li class="item  item2 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=filtroAttrGenero14%3AHomens" class="menu-geral-neto">Homens </a>

                                                    </li>          
                                                    <li class="item  ultimo item2 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=filtroAttrGenero14%3AMulheres" class="menu-geral-neto">Mulheres </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  ultimo item3 menu-geral-filho">

                                                                <span class="menu-geral-filho">Material </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  ultimo item3">
                                                    <li class="item  item3 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=filtroAttrTipodeCouro14%3AAvestruz" class="menu-geral-neto">Avestruz </a>

                                                    </li>          
                                                    <li class="item  item3 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=filtroAttrTipodeCouro14%3ABovino" class="menu-geral-neto">Bovino </a>

                                                    </li>          
                                                    <li class="item  ultimo item3 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=filtroAttrTipodeCouro14%3AJacar%C3%A9" class="menu-geral-neto">Jacaré </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  raiz item0 menu-geral">

                                                            <a href="https://www.crisecia.com.br/homens" class="menu-geral"><img src="https://recursos.crisecia.com.br/Imagem/Menu/298/Homens.png">Homens</a>

                                                            <span class="arrowmenu"></span><ul class="filho  item0">
                                                    <li class="item  item3 menu-geral-filho">

                                                                <span class="menu-geral-filho">Produtos </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  item3">
                                                    <li class="item  item10 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens/produtos/bones" class="menu-geral-neto">Bonés </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens/produtos/calcas" class="menu-geral-neto">Calças </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens/produtos/camisas" class="menu-geral-neto">Camisas </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens/produtos/camisetas-e-polos" class="menu-geral-neto">Camisetas e Polos </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens/produtos/cintos" class="menu-geral-neto">Cintos </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens/produtos/fivelas" class="menu-geral-neto">Fivelas </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens/produtos/jaquetas-e-coletes" class="menu-geral-neto">Jaquetas e Coletes </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens/produtos/moletons" class="menu-geral-neto">Moletons </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens/produtos/shorts-e-bermudas" class="menu-geral-neto">Shorts e Bermudas </a>

                                                    </li>          
                                                    <li class="item  ultimo item10 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens/produtos/tenis" class="menu-geral-neto">Tênis </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  item3 menu-geral-filho">

                                                                <span class="menu-geral-filho">Acessórios </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  item3">
                                                    <li class="item  item5 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens/acessorios/bandanas" class="menu-geral-neto">Bandanas </a>

                                                    </li>          
                                                    <li class="item  item5 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens/acessorios/canivetes" class="menu-geral-neto">Canivetes </a>

                                                    </li>          
                                                    <li class="item  item5 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens/acessorios/carteiras" class="menu-geral-neto">Carteiras </a>

                                                    </li>          
                                                    <li class="item  item5 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens/acessorios/pulseiras" class="menu-geral-neto">Pulseiras </a>

                                                    </li>          
                                                    <li class="item  ultimo item5 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens/acessorios/diversos" class="menu-geral-neto">Diversos </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  ultimo item3 menu-geral-filho">

                                                                <span class="menu-geral-filho">Mais Vendidas </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  ultimo item3">
                                                    <li class="item  item5 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens?busca=&amp;ordenacao=maisVendidos%3Adecrescente&amp;filtro=fabricante1%3ACinch" class="menu-geral-neto">Cinch </a>

                                                    </li>          
                                                    <li class="item  item5 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens?busca=&amp;ordenacao=maisVendidos%3Adecrescente&amp;filtro=fabricante1%3AGringa%27S+Western+Wear" class="menu-geral-neto">Gringa'S Western Wear </a>

                                                    </li>          
                                                    <li class="item  item5 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens?busca=&amp;ordenacao=maisVendidos%3Adecrescente&amp;filtro=fabricante1%3AKing+Farm" class="menu-geral-neto">King Farm </a>

                                                    </li>          
                                                    <li class="item  item5 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens?busca=&amp;ordenacao=maisVendidos%3Adecrescente&amp;filtro=fabricante1%3AOx+Horns" class="menu-geral-neto">Ox Horns </a>

                                                    </li>          
                                                    <li class="item  ultimo item5 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/homens?busca=&amp;ordenacao=maisVendidos%3Adecrescente&amp;filtro=fabricante1%3ATXC" class="menu-geral-neto">TXC </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  raiz item0 menu-geral">

                                                            <a href="https://www.crisecia.com.br/mulheres" class="menu-geral"><img src="https://recursos.crisecia.com.br/Imagem/Menu/299/Mulheres.png">Mulheres</a>

                                                            <span class="arrowmenu"></span><ul class="filho  item0">
                                                    <li class="item  item3 menu-geral-filho">

                                                                <span class="menu-geral-filho">Produtos </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  item3">
                                                    <li class="item  item9 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres/produtos/blusas-e-polos" class="menu-geral-neto">Blusas e Polos </a>

                                                    </li>          
                                                    <li class="item  item9 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres/produtos/bones" class="menu-geral-neto">Bonés </a>

                                                    </li>          
                                                    <li class="item  item9 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres/produtos/calcas-e-shorts" class="menu-geral-neto">Calças e Shorts </a>

                                                    </li>          
                                                    <li class="item  item9 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres/produtos/camisetes" class="menu-geral-neto">Camisetes </a>

                                                    </li>          
                                                    <li class="item  item9 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres/produtos/cintos" class="menu-geral-neto">Cintos </a>

                                                    </li>          
                                                    <li class="item  item9 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres/produtos/fivelas" class="menu-geral-neto">Fivelas </a>

                                                    </li>          
                                                    <li class="item  item9 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres/produtos/jaquetas-e-coletes" class="menu-geral-neto">Jaquetas e Coletes </a>

                                                    </li>          
                                                    <li class="item  item9 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres/produtos/moletons" class="menu-geral-neto">Moletons </a>

                                                    </li>          
                                                    <li class="item  ultimo item9 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres/produtos/tenis" class="menu-geral-neto">Tênis </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  item3 menu-geral-filho">

                                                                <span class="menu-geral-filho">Acessórios </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  item3">
                                                    <li class="item  item6 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres/acessorios/bandanas" class="menu-geral-neto">Bandanas </a>

                                                    </li>          
                                                    <li class="item  item6 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres/acessorios/bolsas" class="menu-geral-neto">Bolsas </a>

                                                    </li>          
                                                    <li class="item  item6 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres/acessorios/canivetes" class="menu-geral-neto">Canivetes </a>

                                                    </li>          
                                                    <li class="item  item6 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres/acessorios/carteiras" class="menu-geral-neto">Carteiras </a>

                                                    </li>          
                                                    <li class="item  item6 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres/acessorios/pulseiras" class="menu-geral-neto">Pulseiras </a>

                                                    </li>          
                                                    <li class="item  ultimo item6 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres/acessorios/diversos" class="menu-geral-neto">Diversos </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  ultimo item3 menu-geral-filho">

                                                                <span class="menu-geral-filho">Mais Vendidas </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  ultimo item3">
                                                    <li class="item  item4 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AMiss+Country" class="menu-geral-neto">Miss Country </a>

                                                    </li>          
                                                    <li class="item  item4 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AOx+Horns" class="menu-geral-neto">Ox Horns </a>

                                                    </li>          
                                                    <li class="item  item4 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres?busca=&amp;ordenacao=&amp;filtro=fabricante1%3ATXC" class="menu-geral-neto">TXC  </a>

                                                    </li>          
                                                    <li class="item  ultimo item4 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/mulheres?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AZenz+Western" class="menu-geral-neto">Zenz Western </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  raiz item0 menu-geral">

                                                            <a href="https://www.crisecia.com.br/infantil" class="menu-geral"><img src="https://recursos.crisecia.com.br/Imagem/Menu/300/Infantil.png">Infantil</a>

                                                            <span class="arrowmenu"></span><ul class="filho  item0">
                                                    <li class="item  ultimo item1 menu-geral-filho">

                                                                <span class="menu-geral-filho">Produtos </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  ultimo item1">
                                                    <li class="item  item6 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/infantil/produtos/bones" class="menu-geral-neto">Bonés </a>

                                                    </li>          
                                                    <li class="item  item6 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/infantil/produtos/botas" class="menu-geral-neto">Botas </a>

                                                    </li>          
                                                    <li class="item  item6 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/infantil/produtos/chapeus" class="menu-geral-neto">Chapéus </a>

                                                    </li>          
                                                    <li class="item  item6 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/infantil/produtos/cintos" class="menu-geral-neto">Cintos  </a>

                                                    </li>          
                                                    <li class="item  item6 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/infantil/produtos/fivelas" class="menu-geral-neto">Fivelas </a>

                                                    </li>          
                                                    <li class="item  ultimo item6 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/infantil/produtos/vestuario" class="menu-geral-neto">Vestuário </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  raiz item0 menu-geral">

                                                            <span class="arrowmenu"></span><a href="#" class="menu-geral"><img src="https://recursos.crisecia.com.br/Imagem/Menu/301/Marcas.png">Marcas</a>

                                                            <ul class="filho  item0">
                                                    <li class="item  item3 menu-geral-filho">

                                                                <span class="menu-geral-filho">Chapéus </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  item3">
                                                    <li class="item  item7 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AAmerican+Hat" class="menu-geral-neto">American Hat </a>

                                                    </li>          
                                                    <li class="item  item7 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3ACharlie+1+Horse" class="menu-geral-neto">Charlie 1 Horse </a>

                                                    </li>          
                                                    <li class="item  item7 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AEldorado" class="menu-geral-neto">Eldorado </a>

                                                    </li>          
                                                    <li class="item  item7 menu-geral-neto">

                                                            <a href="   https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3ALone+Star" class="menu-geral-neto">Lone Star </a>

                                                    </li>          
                                                    <li class="item  item7 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AMexican+Hats" class="menu-geral-neto">Mexican Hats </a>

                                                    </li>          
                                                    <li class="item  item7 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3APralana" class="menu-geral-neto">Pralana </a>

                                                    </li>          
                                                    <li class="item  ultimo item7 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/chapeus?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AResistol" class="menu-geral-neto">Resistol </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  item3 menu-geral-filho">

                                                                <span class="menu-geral-filho">Botas </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  item3">
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AAriat" class="menu-geral-neto">Ariat </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AClassic" class="menu-geral-neto">Classic </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3ADurango" class="menu-geral-neto">Durango </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AGalope+Boots" class="menu-geral-neto">Galope Boots </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AGoyazes" class="menu-geral-neto">Goyazes </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AHelazza+Boots" class="menu-geral-neto">Helazza Boots </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/busca?busca=Botas+jacomo&amp;ordenacao=DataCadastro%3Adecrescente" class="menu-geral-neto">Jácomo </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AJustin+Boots" class="menu-geral-neto">Justin Boots </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AMexican+Boots" class="menu-geral-neto">Mexican Boots </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AMr.+West+Boots" class="menu-geral-neto">Mr. West Boots </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/rosa-ribeiro" class="menu-geral-neto">Rosa Ribeiro </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3ATexas+Boots" class="menu-geral-neto">Texas Boots </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3ATimberland" class="menu-geral-neto">Timberland </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3ATucson" class="menu-geral-neto">Tucson </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AVimar+Boots" class="menu-geral-neto">Vimar Boots </a>

                                                    </li>          
                                                    <li class="item  ultimo item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/botas?busca=&amp;ordenacao=&amp;filtro=fabricante1%3AZebu" class="menu-geral-neto">Zebu </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  ultimo item3 menu-geral-filho">

                                                                <span class="menu-geral-filho">Vestuário </span>

                                                            <span class="arrowmenu arrowmenu-sub"></span><ul class="filho  ultimo item3">
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/cinch" class="menu-geral-neto">Cinch </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/docks" class="menu-geral-neto">Dock's </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/gringas-western-wear" class="menu-geral-neto">Gringa's Western </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/king-farm" class="menu-geral-neto">King Farm </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/levis" class="menu-geral-neto">Levi's </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/made-in-mato" class="menu-geral-neto">Made in Mato </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/marruco" class="menu-geral-neto">Marruco </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/mexican-jeans" class="menu-geral-neto">Mexican Jeans </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/mexican-shirts" class="menu-geral-neto">Mexican Shirts </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/miss-country" class="menu-geral-neto">Miss Country </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/never-give-up" class="menu-geral-neto">Never Give Up </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/tassa" class="menu-geral-neto">Tassa </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/tuff" class="menu-geral-neto">Tuff  </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/txc" class="menu-geral-neto">TXC </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/wrangler" class="menu-geral-neto">Wrangler </a>

                                                    </li>          
                                                    <li class="item  ultimo item16 menu-geral-neto">

                                                            <a href="https://www.crisecia.com.br/fabricante/zenz-western" class="menu-geral-neto">Zenz Western </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  raiz item0 menu-geral">

                                                            <a href="https://www.crisecia.com.br/selaria" class="menu-geral"><img src="https://recursos.crisecia.com.br/Imagem/Menu/302/Selaria.png">Selaria</a>

                                                            <span class="arrowmenu"></span><ul class="filho  item0">
                                                    <li class="item  item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/barrigueiras" class="menu-geral-filho">Barrigueiras </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/cabecadas" class="menu-geral-filho">Cabeçadas </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/cabresto" class="menu-geral-filho">Cabresto </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/conservacao-de-couro" class="menu-geral-filho">Conservação de Couro </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/cordas-lacos-e-peias" class="menu-geral-filho">Cordas, Laços e Peias </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/esporas-e-correias" class="menu-geral-filho">Esporas e Correias </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/freios-e-bridoes" class="menu-geral-filho">Freios e Bridões </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/gamarra" class="menu-geral-filho">Gamarra </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/higiene-e-limpeza" class="menu-geral-filho">Higiene e Limpeza </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/mantas-e-sobremantas" class="menu-geral-filho">Mantas e Sobremantas </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/mochilas-e-bolsas" class="menu-geral-filho">Mochilas e Bolsas </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/peitorais" class="menu-geral-filho">Peitorais </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/protecoes" class="menu-geral-filho">Proteções </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/redeas" class="menu-geral-filho">Rédeas </a>

                                                    </li>          
                                                    <li class="item  item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/selas" class="menu-geral-filho">Selas </a>

                                                    </li>          
                                                    <li class="item  ultimo item16 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/selaria/diversos" class="menu-geral-filho">Diversos </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  raiz item0 menu-geral">

                                                            <a href="https://www.crisecia.com.br/rodeio" class="menu-geral"><img src="https://recursos.crisecia.com.br/Imagem/Menu/303/Rodeio.png">Rodeio</a>

                                                            <span class="arrowmenu"></span><ul class="filho  item0">
                                                    <li class="item  item10 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/rodeio/capacete" class="menu-geral-filho">Capacete </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/rodeio/charrao" class="menu-geral-filho">Charrão </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/rodeio/colete" class="menu-geral-filho">Colete </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/rodeio/cordas" class="menu-geral-filho">Cordas </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/rodeio/esporas-e-correias" class="menu-geral-filho">Esporas e Correias </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/rodeio/luvas-e-munhequeiras" class="menu-geral-filho">Luvas e Munhequeiras </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/rodeio/mochilas-e-bolsas" class="menu-geral-filho">Mochilas e Bolsas </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/rodeio/protetor-bucal" class="menu-geral-filho">Protetor Bucal </a>

                                                    </li>          
                                                    <li class="item  item10 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/rodeio/roupa-salva-vidas" class="menu-geral-filho">Roupa Salva Vidas </a>

                                                    </li>          
                                                    <li class="item  ultimo item10 menu-geral-filho">

                                                            <a href="https://www.crisecia.com.br/rodeio/sedem" class="menu-geral-filho">Sedém </a>

                                                    </li>          
                                                            </ul>
                                                    </li>          
                                                    <li class="item  raiz ultimo item0 menu-geral">

                                                            <a href="http://www.crisecia.com.br/outlet" class="menu-geral"><img src="https://recursos.crisecia.com.br/Imagem/Menu/304/Outlet.png">Outlet</a>

                                                    </li>          
                                                  </ul>



                                                        </div>  
                        </div>

            </div>
        </header>

	<!-- PÁGINA DE NEWSLETTER -->
	<div class="lenteNewsletter"  style="display:none;" >
		<div class="pg pg-newsletter">
			<!-- IMAGEM NEWS -->
			<figure>
				<span class="fecharModalNewsletter" style="display: none;"><i class="fas fa-times"></i></span>
				<img src="<?php echo $configuracao['opt_imagemNewsLetter']['url'] ?>" alt="Cavalo correndo">
			</figure>
			<!-- ÁREA DE CADASTRO -->
			<article>
				<div class="textosArticle">
					<p><i class="far fa-envelope"></i> <strong>Cadastre seu e-mail</strong> e receba as melhores ofertas!</p>
					<input type="text" name="nome" placeholder="Nome">
					<input class="email" type="text" name="email" placeholder="E-mail">
					<input type="submit" name="cadastrar" value="CADASTRAR">
				</div>
			</article>
		</div>
	</div>

	<script>
    setTimeout(function(){
        var elementbtnSearchPesquisa = document.querySelector(".searchBusca #btnSearchPesquisa")
        var elementSearchInput = document.querySelector(".searchBusca input")
        
        elementSearchInput.addEventListener("keypress", function(event){ 
             if (event.which == 13) {
                window.location.href = "https://www.crisecia.com.br/busca?busca="+elementSearchInput.value;
            }
        });
       
        elementbtnSearchPesquisa.addEventListener("click", redirectSearch);
        function redirectSearch(){
            window.location.href = "https://www.crisecia.com.br/busca?busca="+elementSearchInput.value;
        }
    }, 100);

    $( ".buttonMobile" ).click(function() {
      $("div.menu").addClass("openMenu");
    });
     $( "#fecharMenuMobile" ).click(function() {
      $("div.menu").removeClass("openMenu");
    });

     $( ".arrowmenu" ).click(function(e) {
  $(this).toggleClass("openMenuItem");
})
</script>

