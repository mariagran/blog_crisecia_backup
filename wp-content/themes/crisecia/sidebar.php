<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package crisecia
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
	<!-- BARRA LATERAL -->
	<div class="sidebar">
		<!-- DIV NEWSLETTER -->
		<div class="areaNewsletter">
			<!-- IMAGEM ENVELOPE -->
			<span><i class="far fa-envelope"></i></span>
			<p><strong>Cadastre seu e-mail</strong> e receba as melhores ofertas.</p>
			

			<!--START Scripts : this is the script part you can add to the header of your theme-->
			<script type="text/javascript" src="http://blog.crisecia.com.br/wp-includes/js/jquery/jquery.js?ver=2.11"></script>
			<script type="text/javascript" src="http://blog.crisecia.com.br/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.11"></script>
			<script type="text/javascript" src="http://blog.crisecia.com.br/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.11"></script>
			<script type="text/javascript" src="http://blog.crisecia.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.11"></script>
			<script type="text/javascript">
            /* <![CDATA[ */
            var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://blog.crisecia.com.br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
            /* ]]> */
            </script>
            <script type="text/javascript" src="http://blog.crisecia.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.11"></script>
			
			<!--END Scripts-->
			<div class="widget_wysija_cont html_wysija">
				<div id="msg-form-wysija-html5ccc529f1605d-2" class="wysija-msg ajax"></div>
				<form id="form-wysija-html5ccc529f1605d-2" method="post" action="#wysija" class="widget_wysija html_wysija">
				
				    <input type="text" name="wysija[user][email]" class="validate[required,custom[email]]" title="Email" placeholder="Seu e-mail" value="" />
				    <span class="abs-req hidden">
				        <input type="text" name="wysija[user][abs][email]" class="validated[abs][email]" value="" />
				    </span>

					<input class="wysija-submit wysija-submit-field" type="submit" value="cadastrar" />

				    <input type="hidden" name="form_id" value="2" />
				    <input type="hidden" name="action" value="save" />
				    <input type="hidden" name="controller" value="subscribers" />
				    <input type="hidden" value="1" name="wysija-page" />
			    
			        <input type="hidden" name="wysija[user_list][list_ids]" value="1" />
			    
			 	</form>
			</div>
		</div>
	</div>

	<!-- TAGS -->
	<div class="tags">
		<span>tags</span>
		<!-- TAGS -->
		<?php
			$post_tags = get_the_tags();  
		 	
		 	if ( $post_tags ):
			 	foreach( $post_tags as $tag ):
		?>
		<a href="<?php bloginfo('url');?>/tag/<?php echo $tag->slug; ?>"><h3><?php echo $tag->name; ?></h3></a>
		<?php
		  		endforeach;
			endif;
		?>
	</div>

	<!-- AREA PESQUISA -->
	<div class="areaPesquisa">
		<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
			<input type="text" name="s" id="search" placeholder="Pesquise no blog">
			<input type="submit" name="enviar" value="	">
		</form>
	</div>

	<!-- AREA PRODUTOS -->
	<div class="areaProdutos" style="display: none">
		<!-- TITULO PRODUTOS -->
		<h3>produtos</h3>
		<!-- CARROSSEL PRODUTO SIDEBAR -->
		<section class="carrosselProdutos" >
			<div id="carrosselProdutosSidebar" class="owl-Carousel" >
				<?php 	
					$produtos = new WP_Query( array( 'post_type' => 'produtos', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $produtos->have_posts() ) : $produtos->the_post();
		
						// FOTO DESTACADA
						$fotoDestaqueProduto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

						$fotoDestaqueProduto = $fotoDestaqueProduto[0];

						// METABOX
						$precoDoProduto = rwmb_meta('Crisecia_precoProduto');

						$precoTransformado= str_replace(".",",",$precoDoProduto);

						$parcelamentoProduto = rwmb_meta('Crisecia_parcelamentoProduto');

						$descontoProduto = rwmb_meta('Crisecia_descontoProduto');

						$verificacaoDesconto = rwmb_meta('Crisecia_verificacaoCheckboxDesconto');

						$linkDoProduto = rwmb_meta('Crisecia_linkProduto');

						$english_format_number = number_format($number);

						if($verificacaoDesconto != 0){ 
							$precoAtual = ($precoTransformado - (($precoTransformado/100)*$descontoProduto));
							//TRANSFORMAÇÃO DE PONTO PARA VIRGULA
							$precoAtualTransformado = str_replace(".",",",$precoAtual);
						}
				?>
					<!-- ITEM PRODUTO -->
					<div class="item itemCarrosselSidebar">
						<!-- SPAN PORCENTAGEM DE DESCONTO -->
						<?php if($verificacaoDesconto != 0): ?>
							<span class="porcentagemDesconto">- <?php echo $descontoProduto .'%'; ?></span>
						<?php endif; ?>
						<!-- IMAGEM DESTACADA DO PRODUTO -->
						<a href="<?php echo $linkDoProduto; ?>">
							<figure>
								<img src="<?php echo $fotoDestaqueProduto ?>" alt="Imagem produto">
							</figure>
							<!-- NOME DO PRODUTO -->
							<h3 class="nomeDoProduto"><?php echo get_the_title(); ?></h3>
						</a>
						<hr>

						<!-- CLASSIFICAÇÃO DO PRODUTO -->
						<span class="classificacaoProduto">
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
							<i class="fas fa-star"></i>
						</span>

						<!-- PREÇO ORIGINAL -->
						<?php if($verificacaoDesconto != 0): ?>
							<small class="precoSemDesconto">
								R$ <?php echo $precoTransformado; ?>
							</small>
						<?php else:  ?>

							<small class="precoSemDesconto" style="visibility: hidden;">
								R$ <?php echo $precoTransformado; ?>
							</small>

						<?php endif; ?>

						<!-- PREÇO ATUAL -->
						<h4 class="precoAtual">
							<strong>
								R$ <?php echo $precoAtualTransformado; ?>
							</strong> à vista
						</h4>

						<!-- PREÇO PARCELADO -->
						<?php if($parcelamentoProduto > 0):
							$precoParcelado = $precoAtualTransformado/$parcelamentoProduto;

							$precoDaParcela = number_format($precoParcelado, 2, ',', ' ');
						?>
							<small class="precoParcelado">
								ou <?php echo $parcelamentoProduto . 'x'?> de R$ <?php 
								echo ($precoDaParcela); ?> sem juros 
							</small>
						<?php endif; ?>
					</div>
				<?php endwhile; wp_reset_query(); ?>

			</div>
				
			<!-- BOTÕES CARROSSEL SIDEBAR -->
			<div class="botoesCarrosselProdutos">
				<button id="flechaEsquerda" class="flechaEsquerdaCarrosselSidebar"><img src="img/flechaEsquerdaCarrossel.png" alt=""></button>
				<button id="flechaDireita" class="flechaDireitaCarrosselSidebar"><img src="img/FlechaDireitaCarrossel.png" alt=""></button>
			</div>
		</section>
	</div>
