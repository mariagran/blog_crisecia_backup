<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package crisecia
 */

get_header();
?>

	<!-- PÁGINA DE BUSCA -->
	<div class="pg pg-busca"  >
		<div class="containerFull">
			<div class="row">
				<div class="col-sm-12">
					<div class="resultadoBusca">
						<p>
							<?php
								/* translators: %s: search query. */
								printf( esc_html__( 'Resultado da busca por: %s', 'crisecia' ),'' . get_search_query() . '' );
							?>
									
						</p>
					</div>
					<!-- SESSÃO DE POSTS ENCONTRADOS -->
					<section class="sessaoPostsEncontrados">
					 <?php 
					 	if (have_posts()): 
					 	
							while (have_posts()) : 
								the_post();
								
						 		$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$fotoPost = $fotoPost[0]; 

								$categories = get_the_category();

								if($post->post_type == "post"):
					?>
						<!-- POST -->
						<div class="postEncontrado">
							<!-- IMAGEM DESTAQUE DO POST -->
							<a href="<?php echo get_the_permalink(); ?>">
								<figure style="background: url(<?php echo $fotoPost; ?>);">
									<img src="<?php echo $fotoPost; ?>" alt="Imagem">
								</figure>
							</a>
							<!-- INFORMAÇÕES DO POST -->
							<article>
								
								<!-- CATEGORIA DO POST -->
								<?php 
									foreach ($categories as $categories) :
										if ($categories->name != "destaque" && $categories->name != "Sem categoria" && $categories->name != "topo" && $categories->name != "sidebar") :
											$nomeCategoria = $categories->name;
										
									 
								?>
								<a href="<?php echo get_the_permalink(); ?>"><h3><?php echo $nomeCategoria; ?></h3></a>
								<?php endif; endforeach; ?>
								<!-- TITULO DO POST -->
								<a href="<?php echo get_the_permalink(); ?>">
									<h2><?php echo get_the_title(); ?></h2>
								</a>
								<!-- DATA DO POST -->
								<span class="dataDoPost"><?php echo get_the_date('j F, Y'); ?></span>
								<!-- DESCRIÇÃO DO POST -->
								<p>
									<?php customExcerpt(200); ?>
								</p>
							</article>
						</div>

					<?php endif; endwhile; endif;?>
					</section>
				</div>

				<!-- SESSÃO PARA ANUNCIOS -->
				<div class="col-sm-4">
						
				</div>
			</div>
			<!-- DIV PAGINADOR -->
			<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
		</div>
	</div>

<?php
get_footer();
